FROM python:3.8-slim

WORKDIR /root/FR

COPY ${REPO_DIR} ${WORKDIR}

RUN apt-get update && \
    apt-get -y install git build-essential libssl-dev libffi-dev libpq-dev && \
    pip3 install -r requirements.txt
