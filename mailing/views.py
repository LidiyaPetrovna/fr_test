from .serializers import ClientSerializer, MailingSerializer, MailingGeneralSerializer, MessageSerializer, \
    MailingPutSerializer, ClientPutSerializer
from rest_framework import generics, mixins, status
from rest_framework.response import Response
from .models import Client, Mailing
from mailing.tasks import do_send_messages
from django.shortcuts import get_object_or_404


class CreateClientView(mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = ClientSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UpdateClientView(mixins.UpdateModelMixin, generics.GenericAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientPutSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class DeleteClientView(mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class RunMailing(generics.GenericAPIView):
    serializer_class = MailingSerializer  # это здесь нужно потому что свеггер ругается, если этого нет

    def get(self, request):
        do_send_messages.delay()
        return Response('job created successfully')


class CreateMailingView(mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        serializer.save()
        do_send_messages.delay(serializer.data['id'])

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UpdateMailingView(mixins.UpdateModelMixin, generics.GenericAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingPutSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class DeleteMailingView(mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingPutSerializer

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class GeneralMailingStatisticsView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingGeneralSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MessagesByMailingView(mixins.ListModelMixin, generics.GenericAPIView):
    serializer_class = MessageSerializer

    def get(self, request, *args, **kwargs):
        self.queryset = get_object_or_404(Mailing, pk=kwargs['mailing_pk']).messages
        return self.list(request, *args, **kwargs)
