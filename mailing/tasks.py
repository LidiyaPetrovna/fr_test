from .celery import app
from .models import Client, Message, Mailing
from .utilities import send_mailing, send_email_with_mailing_statistics, filter_messages_and_mailings, get_mailing_dicts
from backports.zoneinfo import ZoneInfo
import datetime
from django.utils import timezone


@app.task
def do_send_messages(exact_mailing_pk=None):
    if exact_mailing_pk:
        mailings_to_process = Mailing.objects.filter(pk=exact_mailing_pk)
    else:
        mailings_to_process = Mailing.objects.all()

    for mailing in mailings_to_process:
        client_queryset = Client.objects.all()
        for client in Client.objects.all():
            zone = ZoneInfo(client.timezone)
            client_now = datetime.datetime.now().astimezone(zone)
            dt_launch = mailing.dt_launch.replace(tzinfo=ZoneInfo(client.timezone))
            dt_deadline = mailing.dt_deadline.replace(tzinfo=ZoneInfo(client.timezone))
            if not dt_launch <= client_now <= dt_deadline:
                client_queryset = client_queryset.exclude(pk=client.pk)
            if mailing.custom_filter:
                if 'mobile_operator_codes' in mailing.custom_filter:
                    client_queryset = client_queryset.exclude(pk=client.pk) if client.mobile_operator_code not in \
                        mailing.custom_filter['mobile_operator_codes'] else client_queryset
                if 'tags' in mailing.custom_filter:
                    client_queryset = client_queryset.exclude(pk=client.pk) if not client.tags or not\
                        any([tag in mailing.custom_filter['tags'] for tag in client.tags]) else client_queryset
        for client in client_queryset:
            # если для данных клиента и рассылки нет успешно отправленных сообщений
            # (иначе не надо отсылать, мессадж для данной рассылки и клиента может быть только один)
            if not Message.objects.filter(client=client, mailing=mailing, status=0):

                # если в предыдущий раз произошел еррор, попытаемся еще раз
                if Message.objects.filter(client=client, mailing=mailing, status=-1):
                    error_msg = Message.objects.get(client=client, mailing=mailing, status=-1)
                    try:
                        res = send_mailing(error_msg.id, error_msg.client.phone, error_msg.mailing.text)
                        error_msg.dt_sent = timezone.now()
                        error_msg.save()
                        if res.json()['code'] != -1:
                            error_msg.status = 0
                            error_msg.save()
                    except Exception as e:
                        # за отсутствием логгирования, пишем пока так
                        print('message_id' + str(error_msg.id))
                        print(e)
                else:
                    try:
                        message = Message.objects.create(client=client, mailing=mailing, status=0)
                        res = send_mailing(message.id, message.client.phone, message.mailing.text)
                        if res.json()['code'] == -1:
                            message.status = -1
                            message.save()
                    except Exception as e:
                        print('message_id' + str(message.id))
                        print(e)
                        message.status = -1
                        message.save()

    return "do_send_messages run successfully"


@app.task
def do_send_email_with_statistics():
    messages_queryset, mailings_queryset = filter_messages_and_mailings()
    mailing_dicts = get_mailing_dicts(messages_queryset, mailings_queryset)

    if mailing_dicts:
        out_message = ''
        for mailing_dict in mailing_dicts:
            mailing = mailing_dict['mailing']
            out_message += '===mailing begin===' + '\n'
            out_message += 'mailing.id:' + str(mailing.pk) + '\n'
            out_message += 'mailing.text:' + str(mailing.text) + '\n'
            out_message += 'mailing.custom_filter:' + str(mailing.custom_filter) + '\n'
            out_message += 'mailing.dt_launch:' + str(mailing.dt_launch) + '\n'
            out_message += 'mailing.dt_deadline:' + str(mailing.dt_deadline) + '\n'
            messages_sent = messages_queryset.filter(status=0, mailing=mailing).count()
            messages_error = messages_queryset.filter(status=-1, mailing=mailing).count()
            out_message += 'messages sent:' + str(messages_sent) + '\n'
            out_message += 'messages error:' + str(messages_error) + '\n'
            out_message += '===mailing end===' + '\n'
            out_message += '\n'
    else:
        out_message = 'Ни по одной из рассылок не было отправлено ни одного сообщения'
    send_email_with_mailing_statistics('Статистика по рассылкам на ' + timezone.now().strftime('%Y.%m.%d'), out_message)
    return 'emails successfully sent'
