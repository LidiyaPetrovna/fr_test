import requests
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail
import datetime
from .models import Mailing, Message


def send_mailing(_id, phone, text):
    url = settings.MAILING_TO_SERVER + '/send/' + str(_id)
    obj = {'id': _id, 'phone': phone, 'text': text}
    res = requests.post(url, json=obj, headers={'Authorization': f'Bearer {settings.MAILING_JWT_TOKEN}'},
                        timeout=settings.REQUESTS_TIMEOUT)
    return res


def get_operator_code_from_phone(phone):
    return phone[2:5]


def send_email_with_mailing_statistics(subject, message):
    send_mail(
        subject,
        message,
        settings.EMAIL_SENDER,
        settings.EMAIL_RECIPIENT_LIST,
        fail_silently=False,
    )


def filter_messages_and_mailings():
    messages_queryset = Message.objects.all()
    mailings_queryset = Mailing.objects.none()
    now = timezone.now()
    for message in Message.objects.all():
        if message.dt_sent + datetime.timedelta(hours=24) >= now:
            if message.mailing not in mailings_queryset:
                mailings_queryset |= Mailing.objects.filter(pk=message.mailing.pk)
        else:
            messages_queryset = messages_queryset.exclude(pk=message.pk)
    return messages_queryset, mailings_queryset


def get_mailing_dicts(messages_queryset, mailings_queryset):
    mailing_dicts = []
    for mailing in mailings_queryset:
        messages_queryset_local = messages_queryset.filter(mailing=mailing)
        mailing_dict = {'mailing': mailing, 'messages': []}
        for message in messages_queryset_local:
            mailing_dict['messages'].append(message)
        mailing_dicts.append(mailing_dict)
    return mailing_dicts
