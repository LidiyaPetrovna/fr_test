from rest_framework import serializers
from .models import Client, Mailing, Message
from .utilities import get_operator_code_from_phone
from drf_yasg import openapi


class MailingCustomFilterField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "custom filter",
            "properties": {
                "operator_mobile_codes": openapi.Schema(
                    title="custom_filter operator_mobile_codes",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_STRING)

                ),
                "tags": openapi.Schema(
                    title="custom_filter tags",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_STRING),
                ),
            },
         }


class MailingSerializer(serializers.ModelSerializer):
    custom_filter = MailingCustomFilterField(required=False)

    class Meta:
        model = Mailing
        fields = '__all__'


class MailingPutSerializer(MailingSerializer):
    custom_filter = MailingCustomFilterField(required=True)


class ClientTagsField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_ARRAY,
            "title": "tags",
            "items": openapi.Items(type=openapi.TYPE_INTEGER)
         }


class ClientSerializer(serializers.ModelSerializer):
    tags = ClientTagsField(required=False)
    mobile_operator_code = serializers.CharField(required=False)

    class Meta:
        model = Client
        fields = '__all__'

    def to_internal_value(self, data):
        data_overriden = data.copy()
        if 'phone' in data:
            data_overriden['mobile_operator_code'] = get_operator_code_from_phone(data['phone'])
        return super().to_internal_value(data_overriden)


class ClientPutSerializer(ClientSerializer):
    tags = ClientTagsField(required=True)


class MailingGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = '__all__'

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        mailing = Mailing.objects.get(pk=representation['id'])
        status_0 = mailing.messages.filter(status__exact=0).count()
        status_1 = mailing.messages.filter(status__exact=-1).count()
        representation['messages'] = {'sent': status_0, 'error': status_1}
        return representation


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
