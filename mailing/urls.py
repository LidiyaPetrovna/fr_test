from django.urls import path
from .views import CreateClientView, UpdateClientView, DeleteClientView, CreateMailingView, UpdateMailingView, \
    DeleteMailingView, RunMailing, GeneralMailingStatisticsView, MessagesByMailingView

urlpatterns = [
    path('create_client/', CreateClientView.as_view(), name='create_client_view'),
    path('update_client/<int:pk>/', UpdateClientView.as_view(), name='update_client_view'),
    path('delete_client/<int:pk>/', DeleteClientView.as_view(), name='delete_client_view'),
    path('create_mailing/', CreateMailingView.as_view(), name='create_mailing_view'),
    path('update_mailing/<int:pk>/', UpdateMailingView.as_view(), name='update_mailing_view'),
    path('delete_mailing/<int:pk>/', DeleteMailingView.as_view(), name='delete_mailing_view'),
    path('run_mailing/', RunMailing.as_view(), name='run_mailing'),
    path('general_mailing_statistics/', GeneralMailingStatisticsView.as_view(), name='general_mailing_statistics'),
    path('messages_by_mailing/<int:mailing_pk>/', MessagesByMailingView.as_view(), name='messages_by_mailing')
]
