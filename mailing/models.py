from django.db import models


class Mailing(models.Model):
    dt_launch = models.DateTimeField(verbose_name="дата и время запуска рассылки")
    text = models.TextField(verbose_name="текст сообщения для доставки клиенту")
    custom_filter = models.JSONField(blank=True, null=True, verbose_name="фильтр свойств клиентов, \
                                     на которых должна быть произведена рассылка")
    dt_deadline = models.DateTimeField(verbose_name="дата и время окончания рассылки")

    def __str__(self):
        return str(self.dt_launch) + ' till ' + str(self.dt_deadline)


class Client(models.Model):
    phone = models.CharField(max_length=11, verbose_name="номер телефона клиента")
    mobile_operator_code = models.CharField(max_length=3, verbose_name="код мобильного оператора")
    tags = models.JSONField(null=True, blank=True, verbose_name="теги")
    timezone = models.CharField(max_length=50, verbose_name="временная зона")
    # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

    def __str__(self):
        return str(self.phone) + ' ' + str(self.tags) if self.tags else str(self.phone)


class Message(models.Model):
    STATUS_CHOICES = [
        (0, 'sent'),
        (-1, 'error')
    ]
    dt_sent = models.DateTimeField(verbose_name="дата и время создания (отправки)", auto_now_add=True)
    status = models.IntegerField(verbose_name="статус отправки", choices=STATUS_CHOICES)
    mailing = models.ForeignKey(Mailing, related_name='messages', verbose_name="id рассылки, в рамках которой \
                                было отправлено сообщение", on_delete=models.CASCADE)
    client = models.ForeignKey(Client, related_name='messages', verbose_name="id клиента, которому отправили",
                               on_delete=models.CASCADE)

    def __str__(self):
        return str(self.status) + ' ' + str(self.client.phone) + ' ' + self.mailing.text
