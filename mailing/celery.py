import os
from django.conf import settings
from celery import Celery
from celery.schedules import crontab
from os import environ as os_environ

# Set the default Django settings module for the 'celery' program.
if os_environ.get('DJANGO_SETTINGS_MODULE') is not None:
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', os_environ.get('DJANGO_SETTINGS_MODULE'))
else:
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')

app = Celery('mailing')
app.conf.broker_url = 'redis://' + settings.REDIS_HOST + ':6379/0'


# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'do_send_messages': {
        'task': 'mailing.tasks.do_send_messages',
        'schedule': crontab(**settings.SEND_MESSAGES_EVERY),
    },
    'do_send_email_with_statistics': {
        'task': 'mailing.tasks.do_send_email_with_statistics',
        'schedule': crontab(**settings.SEND_EMAILS_EVERY),
    },
}
