from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from .models import Mailing, Client, Message
import datetime
from django.conf import settings
from backports.zoneinfo import ZoneInfo
from .utilities import get_operator_code_from_phone, filter_messages_and_mailings
from django.test.utils import override_settings
from django.utils import timezone
from django.db.models import Q
from .serializers import MessageSerializer
from mailing.tasks import do_send_messages


class MessagesByMailingTests(APITestCase):
    def test_get_messages_by_mailing(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        url = reverse('messages_by_mailing', kwargs={'mailing_pk': mailing.pk})
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        message_1 = Message.objects.create(mailing=mailing, client=client, status=-1)
        message_2 = Message.objects.create(mailing=mailing, client=client, status=0)
        messages = Message.objects.filter(Q(pk=message_1.pk) | Q(pk=message_2.pk))
        response = self.client.get(url)
        response_json = response.json()
        serializer = MessageSerializer(messages, many=True)
        self.assertEqual(serializer.data, response_json)


class GeneralMailingStatisticsTests(APITestCase):
    def test_get_general_mailing_statistics(self):
        url = reverse('general_mailing_statistics')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        Message.objects.create(mailing=mailing, client=client, status=-1)
        Message.objects.create(mailing=mailing, client=client, status=0)
        response = self.client.get(url)
        response_json = response.json()
        self.assertNotEqual(response_json, [])
        self.assertEqual(response_json[0]['messages'], {'error': 1, 'sent': 1})


class MessageTests(APITestCase):
    @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    def test_run_mailing(self):
        url = reverse('run_mailing')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)

    def test_message_no_tags_no_mobile_operator_codes(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)
        message = Message.objects.get(mailing=mailing, client=client)
        self.assertEqual(message.status, 0)

    def test_message_tags_fits(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         custom_filter={"tags": ["one", "two"]},
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', tags=['one'], timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)
        message = Message.objects.get(mailing=mailing, client=client)
        self.assertEqual(message.status, 0)

    def test_message_tags_doesnt_fit(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         custom_filter={"tags": ["one", "two"]},
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', tags=['1'], timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 0)

    def test_message_mobile_operator_codes_fits(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         custom_filter={"mobile_operator_codes": ['999', '888', '777', '000']},
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='700000000', mobile_operator_code='000', timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)
        message = Message.objects.get(mailing=mailing, client=client)
        self.assertEqual(message.status, 0)

    def test_message_mobile_operator_codes_doesnt_fit(self):
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         custom_filter={"mobile_operator_codes": ['999', '888', '777']},
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 0)

    def test_message_not_in_interval(self):
        Mailing.objects.create(dt_launch="2021-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2021-01-02T00:00:00Z")
        Mailing.objects.create(dt_launch="2024-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-02T00:00:00Z")
        Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        do_send_messages()
        self.assertEqual(Message.objects.all().count(), 0)

    def test_message_timezone_conversion(self):
        client_utc = Client.objects.create(phone='70000000000',
                                           mobile_operator_code='000', timezone='UTC')  # +00:00
        client_Chicago = Client.objects.create(phone='79990000000',
                                               mobile_operator_code='999', timezone='America/Chicago')  # -06:00
        client_Brazzaville = Client.objects.create(phone='78880000000',
                                                   mobile_operator_code='888', timezone='Africa/Brazzaville')  # +01:00
        dt_launch = timezone.now()-datetime.timedelta(minutes=10)
        dt_deadline = timezone.now()+datetime.timedelta(minutes=10)
        Mailing.objects.create(dt_launch=dt_launch, text="mailing text", dt_deadline=dt_deadline)
        do_send_messages()
        all_messages_client_ids = [_id[0] for _id in Message.objects.values_list("client")]
        self.assertIn(client_utc.id, all_messages_client_ids)
        self.assertNotIn(client_Chicago.id, all_messages_client_ids)
        self.assertNotIn(client_Brazzaville.id, all_messages_client_ids)

    @override_settings(MAILING_TO_SERVER='')
    def test_message_error(self):
        client = Client.objects.create(phone='79990000000', mobile_operator_code='999', timezone='America/Chicago')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        do_send_messages()
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)
        message = Message.objects.filter(mailing=mailing, client=client).first()
        self.assertEqual(message.status, -1)

    def test_message_already_sent(self):
        client = Client.objects.create(phone='79990000000', mobile_operator_code='999', timezone='America/Chicago')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        Message.objects.create(mailing=mailing, client=client, status=0)
        do_send_messages()
        self.assertEqual(Message.objects.all().count(), 1)

    def test_message_correct_error(self):
        client = Client.objects.create(phone='79990000000', mobile_operator_code='999', timezone='America/Chicago')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        message = Message.objects.create(mailing=mailing, client=client, status=-1)
        do_send_messages()
        message = Message.objects.get(pk=message.pk)
        self.assertEqual(message.status, 0)


class ClientTests(APITestCase):
    def test_create_client(self):
        url = reverse('create_client_view')
        phone = '79999999999'
        timezone_field = 'America/Chicago'
        data = {"phone": phone, "timezone": timezone_field}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.filter(pk=response.json()['id']).count(), 1)
        client = Client.objects.get(pk=response.json()['id'])
        self.assertEqual(client.phone, phone)
        self.assertEqual(client.mobile_operator_code, get_operator_code_from_phone(phone))
        self.assertEqual(client.timezone, timezone_field)

    def test_update_client(self):
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        url = reverse('update_client_view', kwargs={'pk': client.pk})
        phone = '79999999999'
        tags = ["one", "two"]
        timezone_field = 'America/Chicago'
        data = {"phone": phone, "tags": tags, "timezone": timezone_field}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        client = Client.objects.get(pk=client.pk)
        self.assertEqual(client.phone, phone)
        self.assertEqual(client.mobile_operator_code, get_operator_code_from_phone(phone))
        self.assertEqual(client.tags, tags)
        self.assertEqual(client.timezone, timezone_field)

    def test_delete_client(self):
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        url = reverse('delete_client_view', kwargs={'pk': client.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_client_fields_required_post(self):
        url = reverse('create_client_view')
        phone = '79999999999'
        tags = ["one", "two"]
        timezone_field = 'America/Chicago'
        data = {"tags": tags, "timezone": timezone_field}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"tags": tags, "phone": phone}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_client_fields_required_put(self):
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        url = reverse('update_client_view', kwargs={'pk': client.pk})
        phone = '79999999999'
        tags = ["one", "two"]
        timezone_field = 'America/Chicago'
        data = {"tags": tags, "timezone": timezone_field}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"tags": tags, "phone": phone}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"phone": phone, "timezone": timezone_field}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class MailingTests(APITestCase):
    def test_create_mailing(self):
        url = reverse('create_mailing_view')
        dt_launch = "2022-01-01T00:00:00"
        text = "qwerty"
        dt_deadline = "2024-01-01T00:00:00"
        data = {"dt_launch": dt_launch, "text": text, "dt_deadline": dt_deadline}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.filter(pk=response.json()['id']).count(), 1)
        mailing = Mailing.objects.get(pk=response.json()['id'])
        self.assertEqual(mailing.dt_launch,
                         datetime.datetime.strptime(dt_launch,
                                                    '%Y-%m-%dT%H:%M:%S').replace(tzinfo=ZoneInfo(settings.TIME_ZONE)))
        self.assertEqual(mailing.text, text)
        self.assertEqual(mailing.dt_deadline,
                         datetime.datetime.strptime(dt_deadline,
                                                    '%Y-%m-%dT%H:%M:%S').replace(tzinfo=ZoneInfo(settings.TIME_ZONE)))

    def test_update_mailing(self):
        mailing = Mailing.objects.create(dt_launch="2000-01-01T00:00:00Z", text="", dt_deadline="2001-01-01T00:00:00Z")
        url = reverse('update_mailing_view', kwargs={'pk': mailing.pk})
        dt_launch = "2022-12-03T11:55:20"
        text = "qwerty"
        custom_filter = {"mobile_operator_codes": ['999', '888', '777'], "tags": ["one", "two"]}
        dt_deadline = "2022-12-05T19:55:20"
        data = {"dt_launch": dt_launch, "text": text,
                "custom_filter": custom_filter, "dt_deadline": dt_deadline}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mailing = Mailing.objects.get(pk=mailing.pk)
        self.assertEqual(mailing.dt_launch,
                         datetime.datetime.strptime(dt_launch,
                                                    '%Y-%m-%dT%H:%M:%S').replace(tzinfo=ZoneInfo(settings.TIME_ZONE)))
        self.assertEqual(mailing.text, text)
        self.assertEqual(mailing.custom_filter, custom_filter)
        self.assertEqual(mailing.dt_deadline,
                         datetime.datetime.strptime(dt_deadline,
                                                    '%Y-%m-%dT%H:%M:%S').replace(tzinfo=ZoneInfo(settings.TIME_ZONE)))

    def test_delete_mailing(self):
        mailing = Mailing.objects.create(dt_launch="2000-01-01T00:00:00Z", text="", dt_deadline="2001-01-01T00:00:00Z")
        url = reverse('delete_mailing_view', kwargs={'pk': mailing.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_mailing_fields_required_post(self):
        url = reverse('create_mailing_view')
        dt_launch = "2022-01-01T00:00:00"
        text = "qwerty"
        dt_deadline = "2024-01-01T00:00:00"
        data = {"text": text, "dt_deadline": dt_deadline}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"dt_launch": dt_launch, "dt_deadline": dt_deadline}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"dt_launch": dt_launch, "text": text}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_mailing_fields_required_put(self):
        mailing = Mailing.objects.create(dt_launch="2000-01-01T00:00:00Z", text="", dt_deadline="2001-01-01T00:00:00Z")
        url = reverse('update_mailing_view', kwargs={'pk': mailing.pk})
        dt_launch = "2022-01-01T00:00:00"
        text = "qwerty"
        custom_filter = {"mobile_operator_codes": ['999', '888', '777'], "tags": ["one", "two"]}
        dt_deadline = "2024-01-01T00:00:00"
        data = {"text": text, "dt_deadline": dt_deadline, "custom_filter": custom_filter}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"dt_launch": dt_launch, "dt_deadline": dt_deadline, "custom_filter": custom_filter}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"dt_launch": dt_launch, "text": text, "custom_filter": custom_filter}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {"dt_launch": dt_launch, "text": text, "dt_deadline": dt_deadline}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    def test_create_message_on_create_mailing(self):
        client = Client.objects.create(phone='70000000000', mobile_operator_code='000', timezone='UTC')
        url = reverse('create_mailing_view')
        dt_launch = "2022-01-01T00:00:00"
        text = "qwerty"
        dt_deadline = "2024-01-01T00:00:00"
        data = {"dt_launch": dt_launch, "text": text, "dt_deadline": dt_deadline}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.filter(pk=response.json()['id']).count(), 1)
        mailing = Mailing.objects.get(pk=response.json()['id'])
        self.assertEqual(Message.objects.filter(mailing=mailing, client=client).count(), 1)
        message = Message.objects.filter(mailing=mailing, client=client).first()
        self.assertEqual(message.status, 0)


class EmailTests(APITestCase):
    def test_filter_messages_and_mailings_in(self):
        client = Client.objects.create(phone='79990000000', mobile_operator_code='999', timezone='America/Chicago')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        Message.objects.create(mailing=mailing, client=client, status=0)
        messages_queryset, mailings_queryset = filter_messages_and_mailings()
        self.assertEqual(messages_queryset.count(), 1)
        self.assertEqual(mailings_queryset.count(), 1)

    def test_filter_messages_and_mailings_out(self):
        client = Client.objects.create(phone='79990000000', mobile_operator_code='999', timezone='America/Chicago')
        mailing = Mailing.objects.create(dt_launch="2022-01-01T00:00:00Z",
                                         text="22-24", dt_deadline="2024-01-01T00:00:00Z")
        m = Message.objects.create(mailing=mailing, client=client, status=0, dt_sent=None)
                                   # dt_sent=timezone.now()-datetime.timedelta(days=2))
        m.dt_sent = timezone.now() - datetime.timedelta(days=2)
        m.save()
        messages_queryset, mailings_queryset = filter_messages_and_mailings()
        self.assertEqual(messages_queryset.count(), 0)
        self.assertEqual(mailings_queryset.count(), 0)
