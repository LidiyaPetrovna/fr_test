from .base import *

DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mailing',
        'USER': 'postgres',
        'PASSWORD': 'as;dlkwqd]',
        'HOST': 'localhost',
        'PORT': '5500',
    }
}

MAILING_TO_SERVER = 'https://probe.fbrq.cloud/v1'
MAILING_JWT_TOKEN = env('MAILING_JWT_TOKEN')

REDIS_HOST = 'localhost'

REQUESTS_TIMEOUT = 5

# celery beat automatic mailing sending
SEND_MESSAGES_EVERY = {'minute': '*/1'}

SEND_EMAILS_EVERY = {'minute': '*/1'}

EMAIL_RECIPIENT_LIST = ['amirny2205@yandex.ru']
EMAIL_SENDER = 'amirny2205@mail.ru'

EMAIL_USE_TLS = False
EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.mail.ru'
EMAIL_PORT = 465
EMAIL_HOST_USER = EMAIL_SENDER
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = 'amirny2205@mail.ru'
